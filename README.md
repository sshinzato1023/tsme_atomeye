# 尾方研Atomeyeマニュアル #

Atomeyeの操作方法を日本語でメモしていくやつです。

## 基本事項 ##

* 親ファイルは**Aman.tex**です。
* 1機能1ファイルを目安に編集してください。
* ファイルはcontentsディレクトリ内に入れといてください
* Webの英文マニュアル直訳も大歓迎!!!

## 編集ルール

マニュアルファイルは以下の要領で一つの機能につきファイルを一つ作成

```tex
%! TEX root = Aman.tex
\section{任意パラメータによる色付け}
\label{sec:AuxPropColor}
\subsection{着色に使うパラメータの選択}
\label{sec:AuxPropColor:Param}
(以下操作方法)
```

* ファイル名はマニュアルにある場合は**見出しの名前.tex**とする
* sectionには**sec:**で始まるラベルを付ける(名称は自由)
* subsection以下はsectionラベル以降':'(コロン)で区切る
* 手順はitemize環境で番号付き箇条書き
* ファイル作成後，commit前に以下のファイルも編集すべし
    1. **Aman.tex**に作成したファイルを読み込むためのinput文を追加する
	2. **ObjectIndex.tex**に目的別でリンクできるようにリンクを埋め込む
* 作成後は一度コンパイルして正常にpdfが生成できることを確認してからcommitすべし
* 簡単な操作は**Misc.tex**に入れるのも可
* エンコードはUTH-8で